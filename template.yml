# This file is a template.
# This CI/CD configuration provides a standard pipeline for:
# * building and testing a python library or package
# * deploying the python library to an internal pypi repository or to pypi.org
#
# This CI/CD configuration is meant to be used in conjunction with the cookiecutter
# template at `https://gitlab.com/byuhbll/lib/python/python-project-template`.
#
# This pipeline expects the following environment variables to be defined:
#---------------------------------------------------------------------------
# Defined at the group level:
#   PIP_CACHE_DIR: The location of the pip cache to use.
#   LOCAL_PYPI_USERNAME: The username used to access internal pypi.
#   LOCAL_PYPI_PASSWORD: The password used to access internal pypi.
#   LOCAL_PYPI_REPO: The host for internal pypi.
#   PUBLIC_PYPI_USERNAME: The username used to access public pypi.
#   PUBLIC_PYPI_PASSWORD: The password used to access public pypi.
#   PUBLIC_PYPI_REPO: The host for public pypi.
#   REGISTRY_HOST: The host for the registry (application information system).
#---------------------------------------------------------------------------
# Defined at the project level or in a .gitlab-ci.yml file:
#   REGISTRY_PROJECT_SLUG: The project slug in the registry to use when
#     registering deployments.
#---------------------------------------------------------------------------
# Defined in a .gitlab-ci.yml file:
#   TARGET_PYTHON_IMAGE: The python image and version to target. Default to 
#     `python:3.8`. 
#   HAS_PEX: Whether or not the project should run the pex build job. Valid values
#     are "true" or "false". Default to "false"
#   USE_PUBLIC_PYPI: Whether or not the project should deploy to the public pypi.
#     Valid values are "true" or "false". Default to "false"
#   PYTHON_VERSIONS: Comma seperated string of python versions supported by this
#     package. This is used in the register deploy process. Default to "3.7,3.8"
#---------------------------------------------------------------------------
# This pipeline also depends on the following environment variables provided by
# GitLab CI automatically:
#   CI_PROJECT_PATH_SLUG
#   CI_COMMIT_TAG
#   CI_COMMIT_REF_NAME
#   GITLAB_USER_LOGIN 
#
# These are defined here:
# https://docs.gitlab.com/ee/ci/variables/predefined_variables.html

image: python:3.8

variables:
  TARGET_PYTHON_IMAGE: python:3.8
  PYTHON_VERSIONS: "3.7,3.8"
  HAS_PEX: "false"
  USE_PUBLIC_PYPI: "false"

cache:
  paths:
    - "$PIP_CACHE_DIR"

# Stages
stages:
  - check
  - build
  - deploy

#----------------------------------------------------------------------------
# check stage jobs
#----------------------------------------------------------------------------
.tox-test:
  stage: check
  image: $TARGET_PYTHON_IMAGE
  script:
    - pip install tox
    - pyversion=`python -c "print('py'+'${TARGET_PYTHON_IMAGE}'.split(':')[1].split('-')[0].replace('.', ''))"`
    - tox -e $pyversion
  coverage: "/TOTAL.*\\s(\\d+)%/"
  artifacts:
    reports:
      junit: junit-report.xml
    
test:
  extends: .tox-test
  
#----------------------------------------------------------------------------
# build stage jobs
#----------------------------------------------------------------------------
.release:
  only:
    refs:
      - /^\d+\.\d+\.\d+$/

.prerelease:
  when: manual
  only:
    refs:
      - branches
      - /^\d+\.\d+\.\d+rc\d+$/
  except:
    refs:
      - master

.package-wheel:
  stage: build
  script:
    - python setup.py bdist_wheel
  artifacts:
    name: "$CI_PROJECT_PATH_SLUG-$CI_COMMIT_TAG-wheel"
    paths:
      - dist/*.whl
    expire_in: 1 week

release:
  extends: 
    - .release
    - .package-wheel
    
prerelease:
  extends:
    - .prerelease
    - .package-wheel
  
.build-pex:
  stage: build
  only:
    variables:
      - $HAS_PEX == "true"
  script:
    - pip install tox
    - tox -e package
  artifacts:
    name: "$CI_PROJECT_PATH_SLUG-$CI_COMMIT_TAG.pex"
    paths:
      - "*.pex"
      
release-pex:
  extends:
    - .release
    - .build-pex

prerelease-pex:
  extends:
    - .prerelease
    - .build-pex
    
#----------------------------------------------------------------------------
# deploy stage jobs
#----------------------------------------------------------------------------
.deploy:
  stage: deploy
  extends: .release
  dependencies:
    - release
  script:
    - pip install twine
    # Upload package to local PyPI using Twine
    - twine upload --repository-url $PYPI_REPO dist/*
    # Register the deployment with Registry
    - register_deployment

internal:
  extends: .deploy
  variables:
    TWINE_USERNAME: $LOCAL_PYPI_USERNAME
    TWINE_PASSWORD: $LOCAL_PYPI_PASSWORD
    PYPI_REPO: $LOCAL_PYPI_REPO

public:
  extends: .deploy
  variables:
    TWINE_USERNAME: $PUBLIC_PYPI_USERNAME
    TWINE_PASSWORD: $PUBLIC_PYPI_PASSWORD
    PYPI_REPO: $PUBLIC_PYPI_REPO
  only:
    variables:
      - $USE_PUBLIC_PYPI == "true"

.bash_functions: &bash_functions |
  function register_deployment(){
    set -ex
    # prepare data
    data="{\"project\":\"${REGISTRY_PROJECT_SLUG}\",\"deployer\":\"${GITLAB_USER_LOGIN}\",\"version\":\"${CI_COMMIT_REF_NAME}\",\"language\":\"Python\",\"language_version\":\"${PYTHON_VERSIONS}\",\"notes\":\"CI/CD deploy.\"}"

    # register deployment in the registry and save the status code
    status_code=$(\
        curl -s -o /dev/null -w "%{http_code}" \
          --header "Content-Type: application/json" \
          --request POST \
          --data "$data" \
          --user "$REGISTRY_USER:$REGISTRY_PASSWORD" \
          $REGISTRY_HOST/v1/deployments/
    )

    # check that registration was successful
    if [[ "$status_code" -ne "201" ]]; then exit 1; fi
  }

before_script:
  - *bash_functions
